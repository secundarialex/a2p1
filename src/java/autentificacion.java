/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import p1.Usuario;

/**
 *
 * @author alex
 */
@WebServlet(urlPatterns = {"/autentificacion"})
public class autentificacion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession ses = request.getSession();
        try (PrintWriter out = response.getWriter()) {
            boolean existe = false;
            String mens = "";
            p1.Usuario buscado = null;
            p1.ListadoUsuarios lista = (p1.ListadoUsuarios) ses.getAttribute("lista");

            if (request.getParameter("usuario").equals("")
                    || request.getParameter("pass").equals("")) {
                mens = "Introduce usuario contraseña";

            } else {
                Iterator<Usuario> it = lista.iterator();
                while (!existe && it.hasNext()) {
                    buscado = it.next();

                    if (request.getParameter("usuario").equals(buscado.getNombre())
                            && request.getParameter("pass").equals(buscado.getPassword())) {
                        existe = true;
                    }
                }
                if (existe) {
                    ses.setAttribute("buscado", buscado);
                    mens = "usuario buscado" + buscado.getNombre();
                    out.println(mens);
                } else {
                    mens = "usuario incorrecto";
                }

            }

            ses.setAttribute("mensaje", mens);
            if(existe) request.getRequestDispatcher("VistaCliente.jsp").forward(request, response);
           else request.getRequestDispatcher("index.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
